/**
 *   Class that prepares the hashmap to be sent to the database using the DAO class
 *   @author: Joppe Baalhuis, Laurens Pelgrom
 *   @version: 1.0
 **/

import java.util.HashMap;

public class DBHandler {
    /**
     * Prepares the values from the LoPy to be put in the database
     * @name:        writeData
     * @param:       HashMap<String, String> dataMap
     * @return:      void
     **/
    public void writeData(HashMap<String, String> dataMap) {

        String deviceId = dataMap.get("dev_id");
        String timeStamp = TimestampConverter.convertToDBDataTimeFormat(dataMap.get("timestamp"));
        String temperature = dataMap.get("temperature");
        String humidity = dataMap.get("humidity");
        String lux_red = dataMap.get("lux_red");
        String lux_blue = dataMap.get("lux_blue");

        DBManager dbmanager = DBManager.getInstance();
        DAO dao =  DAO.getInstance(dbmanager);

        //calling func to write to database
        dao.writeData(deviceId, timeStamp, temperature, humidity, lux_red, lux_blue);
    }
}