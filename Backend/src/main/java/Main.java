/**
 *   Main class that handles getting the data from the TTN continuously.
 *   Besides, it instantiates the socket listener and the DB connection.
 *   @author: Arsenii Belyakov, Joppe Baalhuis, Laurens Pelgrom, Timo van der Kuil
 *   @version: 1.0
 **/

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Main
{
    /**
     * main of the program
     * @name:        main
     * @param:       String[] args
     * @return:      void
     */
    public static void main(String[] args) {

        //Info to connect to the MQTTBroker
        String broker = "tcp://eu.thethings.network:1883";
        String clientId = "spongeweather2000maxTimo";
        String pass = "ttn-account-v2.69z7GLOAFYRn6MMdxYgdDGrwVYrLGc3YEKIiJ-qdaCI";
        String user = "spongeweather2000max";

        //flag to break while loop if necessary
        boolean flag = true;

        //making parser and connection objects
        JSONParser parser = new JSONParser();
        MQTTConnect conn = new MQTTConnect(broker, pass, user, clientId);

        //Making the database connection object and DAO object
        DBManager dbManager = DBManager.getInstance();
        DAO DAO1 = DAO.getInstance(dbManager);
        DBHandler DBHandler = new DBHandler();

        //sets up the listenersocket
        ListenerSocket listenerSocket = new ListenerSocket(DAO1);

        //starting connection to TTN
        conn.startSubscriptionMQTT();

        //starting server where clients connect
        listenerSocket.startServer();

        //While loop to keep checking for new data
        while (flag)
        {
            //Hashmap containing parsed data
            HashMap<String, String> parsedData;

            //string containing unparsed data
            String unparsedData = conn.getData();

            //delay to prevent read/write collisions
            try
            {
                TimeUnit.SECONDS.sleep(5);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            //only if there is data, call the parser
            if (unparsedData != null)
            {
                //converting JSON data to hashmap
                parser.covertData(unparsedData);

                //Here the parsedData is returned
                parsedData = parser.getData();

                //sending the parsed data to be written to the database
                DBHandler.writeData(parsedData);

            }
        }
    }
}
