/**
 *   Class that subscribes to the MQTT broker of TTN, also receives the data
 *   @author: Timo van der Kuil
 *   @version: 1.0
 **/

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTTConnect {
    private MqttClient sampleClient;
    private String loPyData;
    private String prev_loPyData = " ";

    /**
     * creates an connection with the TTN
     * @name:        MQTTConnect
     * @param:       String broker, String pass, String user, String clientId
     * @return:      void
     */
    MQTTConnect(String broker, String pass, String user, String clientId) {
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();

            //setting connoptions
            connOpts.setPassword(pass.toCharArray());
            connOpts.setUserName(user);
            connOpts.setConnectionTimeout(60);
            connOpts.setKeepAliveInterval(60);
            connOpts.setCleanSession(true);

            //connecting
            sampleClient.connect(connOpts);
            System.out.println("Connection with MQTTBroker established.");

            //subscribing to topic
            sampleClient.subscribe("+/devices/+/up");

        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }

    /**
     * creates an listener of sorts, to return the received data from the MQTTBroker of TTN
     * @name:        startSubscriptionMQTT
     * @param:       void
     * @return:      void
     *
     */
    public void startSubscriptionMQTT() {
        //Setting the callback to the MQTTBroker
        sampleClient.setCallback(new MqttCallback() {
            public void connectionLost(Throwable cause) {
            }
            //If a message is received, save this data to a string
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                //if there's a message, put it in loPyData
                loPyData = message.toString();
            }

            public void deliveryComplete(IMqttDeliveryToken token) {
            }
        });
    }

    /**
     * Gets the data from the listener, and checks if it's same to the previous.
     * @name:        getData
     * @param:       none
     * @return:      String
     *
     */
    public String getData(){
        //if there is no data, set " "
        //else the if statements has a nullpointerexception
        if(loPyData == null){
            loPyData = " ";
        }

        //If the current data is NOT the same
        //as previous data return it
        if(!loPyData.equals(prev_loPyData)){
            prev_loPyData = loPyData;
            return loPyData;
        }
        prev_loPyData = loPyData;
        return null;
    }
}

