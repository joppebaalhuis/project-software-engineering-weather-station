/**
 *   Class that handles reading and writing to the database,
 *   actually sending the SQL statements
 *   @author: Joppe Baalhuis, Laurens Pelgrom, Timo van der Kuil
 *   @version: 1.0
 **/

import java.sql.*;
import java.util.*;

public class DAO
{
    private static DAO uniqueInstance = null;
    private static Connection connection = null;

    /**
     * setup the connection with the DB if there is no connection
     * @name:        DAO
     * @param:       DBManager db
     * @return:      void
     */
    private DAO(DBManager db) {
        if ((connection = db.getConnection()) == null) {
            System.err.println(" The database doesn't exist ...");
        }
    }

    /**
     * apply singleton design pattern to
     * setup the connection with the DB (Consgtructor)
     * @name:        getInstance
     * @param:       DBManager db
     * @return:      DAO
     */
    public static synchronized DAO getInstance(DBManager db) {
        if (uniqueInstance == null) {
            uniqueInstance = new DAO(db);
        }
        return uniqueInstance;
    }

    /**
     * Makes the prepared statement and executes it to write data to the database
     * @name:        writeData
     * @param:       String deviceId, String timestamp, String temperature, String humidity, String lux_red, String lux_blue
     * @return:      void
     */
    public void writeData(String deviceId, String timestamp, String temperature, String humidity, String lux_red, String lux_blue) {
        PreparedStatement preparedStatement2 = null;
        try {
            //This statement inserts all data into the database
            preparedStatement2 = connection.prepareStatement("INSERT INTO Recived_Data(device_id, timestamp, temperature, humidity, lux_red, lux_blue) VALUES(?,?,?,?,?,?)");

            preparedStatement2.setString(1,  deviceId);
            preparedStatement2.setString(2,  timestamp);
            preparedStatement2.setString(3,  temperature);
            preparedStatement2.setString(4,  humidity);
            preparedStatement2.setString(5,  lux_red);
            preparedStatement2.setString(6,  lux_blue);

            preparedStatement2.execute();
            System.out.println("LoPy data written to database.");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement2.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets the latest added value from the database
     * @name:        getLatestDBdata
     * @param:       none
     * @return:      HashMap<String, String>
     */
    public TreeMap<String, String> getLatestDBdata()
    {
        TreeMap<String, String> dataFromDB = new TreeMap<>();
        //This statement gets only the latest entry in the database, to limit data to db and to client
        String query = "select * from Recived_Data ORDER BY counter DESC LIMIT 1;";

        //Executing the query
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(query);

            //putting the result in the TreeMap
            while(result.next()) {
                dataFromDB = putDataInTree(result);
                dataFromDB.put("timestamp", result.getString("timestamp"));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return dataFromDB;
    }

    /**
     * Gets all the data that is already in the database. This is used for the first query.
     * @name:        getAllData
     * @param:       none
     * @return:      TreeMap<String, TreeMap<String, String>>
     */
    public TreeMap<String, TreeMap<String, String>> getAllData() {
        TreeMap<String, TreeMap<String, String>> dataMap = new TreeMap<>();
        try {
            Statement statement = connection.createStatement();

            //executing query
            ResultSet result = statement.executeQuery("select * from Recived_Data");

            //putting result in TreeMap
            while (result.next()) {
                dataMap.put(result.getString("timestamp"), putDataInTree(result));
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

        return dataMap;
    }
    /**
     * Puts the data in the tree to be nested in another tree in the getAllData() and getLatestDBdata funcs.
     * @name:        getAllData
     * @param:       Resultset result
     * @return:      TreeMap<String, String>
     */
    TreeMap<String, String> putDataInTree(ResultSet result){
        TreeMap<String, String> data = new TreeMap<>();
        try {
            data.put("Device_id", result.getString("device_id"));
            data.put("counter", result.getString("counter"));
            data.put("temperature", result.getString("temperature"));
            data.put("humidity", result.getString("humidity"));
            data.put("lux_red", result.getString("lux_red"));
            data.put("lux_blue", result.getString("lux_blue"));
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return data;
    }
}
