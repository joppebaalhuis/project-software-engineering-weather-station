/**
 *   Class that handles the client sockets.
 *   Also handles the commands received to get data to the client
 *   @author: Joppe Baalhuis
 *   @version: 1.0
 **/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ListenerSocket {
    private DAO dao;

    /**
     * assigns the global DAO value
     * @name:        ListenerSocket
     * @param:       DAO
     * @return:      void
     */
    public ListenerSocket(DAO param)
    {
        dao = param;
    }

    //lookup table with all the available commands, this way it is easy to extend
    // and we can use switch casing with strings
    private HashMap<String,Integer> commandLookupTable = new HashMap<String,Integer>();

    /**
     * Puts all known commands in a lookuptable
     * @name:        fillCommandLookupTable
     * @param:       none
     * @return:      void
     */
    void fillCommandLookupTable() {
        commandLookupTable.put("getAllDevices", 0);
        commandLookupTable.put("getLatestData",1);
        commandLookupTable.put("ping", 2);
        commandLookupTable.put("getAllData",3);
    }

    /**
     * Starts the server with a thread pool of 10
     * @name:        ClientTask
     * @param:       none
     * @return:      void
     */
    public void startServer() {
        final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(10);

        //filling the lookup table
        fillCommandLookupTable();
        Runnable serverTask = new Runnable() {
            @Override
            public void run() {
                try {
                    ServerSocket serverSocket = new ServerSocket(8000);
                    System.out.println("Waiting for clients to connect...");
                    while (true) {
                        Socket clientSocket = serverSocket.accept();
                        clientProcessingPool.submit(new ClientTask(clientSocket));
                    }
                }
                catch (IOException e) {
                    System.err.println("Unable to process client request");
                    e.printStackTrace();
                }
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }


    /**
     *   private class that handles the client requests
     *   @author: Joppe Baalhuis
     *   @version: 1.0
     **/
    private class ClientTask implements Runnable {
        private final Socket clientSocket;

        /**
         * Assigns the Socket variable
         * @name:        ClientTask
         * @param:       Socket
         * @return:      void
         */
        private ClientTask(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
        }

        /**
         * The code used for every thread
         * @name:        run
         * @param:       none
         * @return:      void
         */
        @Override
        public void run() {
            System.out.println("Client connection established.");

            try {
                //Get Data From Client
                BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String clientData = "";
                clientData = reader.readLine();

                //create the right responseMessage
                String responseMessage = clientCommandParser(clientData);

                //send the response to the client
                OutputStream os = clientSocket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
                bw.write(responseMessage+ "\n");
                System.out.println("Message sent to client.");
                bw.flush();
                clientSocket.close();

            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * interprets the command that is received from the client and determines how to respond
     * @name:        ClientCommandParser
     * @param:       String
     * @return:      String
     */
    private String clientCommandParser(String clientCommandString) {
        String returnMessageString = "";
        String param1 = "";

        //accommodating for command + parameters
        String[] clientCommandArray = clientCommandString.split(" ");
        String command  = clientCommandArray[0];

        //if the command has parameters, read them
        if(clientCommandArray.length > 1) {
            param1  = clientCommandArray[1];
        }

        //decide what to return back
        switch (commandLookupTable.get(command)) {
            case 0:     returnMessageString = "this command is not implemented yet";
                break;
            case 1:     returnMessageString = createResponseJSONFromTreeMap(dao.getLatestDBdata()).toString();
                break;
            case 2:     returnMessageString = "pong";
                break;
            case 3:     returnMessageString = createResponseJSONFromNestedTreeMap(dao.getAllData()).toString();
                break;
            default:    returnMessageString = "bad request";
                break;
        }
        return returnMessageString;
    }

    /**
     * creates a JSONArray from all the data in the TreeMap
     * @name:        createResponseJSONFromNestedTreeMap
     * @param:       TreeMap<String, TreeMap<String,String>>
     * @return:      JSONArray
     */
    private JSONArray createResponseJSONFromNestedTreeMap(TreeMap<String, TreeMap<String,String>> dataFromDB) {
        JSONArray array = new JSONArray();
        try {
            for (Map.Entry<String, TreeMap<String, String>> x : dataFromDB.entrySet()) {
                JSONObject returnMessage = new JSONObject();
                JSONObject data = new JSONObject();

                String key = x.getKey();
                TreeMap<String, String> value = x.getValue();

                for (Map.Entry<String, String> t : value.entrySet()) {
                    data.put(t.getKey(), t.getValue());
                }
                returnMessage.put("Timestamp", key);
                returnMessage.put("Data", data);
                array.put(returnMessage);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return  array;
    }
    /**
     * creates a JSONObject from the treemap with the latest data.
     * @name:        createResponseJSONFromTreeMap
     * @param:       TreeMap<String,String>
     * @return:      JSONObject
     */
    private JSONObject createResponseJSONFromTreeMap(TreeMap<String,String> dataFromDB) {
        JSONObject obj = new JSONObject();
        try {
                for (Map.Entry<String, String> t : dataFromDB.entrySet()) {
                    obj.put(t.getKey(), t.getValue());
                }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return  obj;
    }
}
