/**
 *   Class that converts timestamps
 *   @author: Laurens Pelgrom
 *   @version: 1.0
 **/

public class TimestampConverter {

    /**
     * static function that converts EPOCH time to SQL's DATETIME format
     * @name:        convertToDBDataTimeFormat
     * @param:       String
     * @return:      String
     */
    static public String convertToDBDataTimeFormat(String EpochtimeStamp) {
        long epoch = Long.parseLong(EpochtimeStamp);

        return new java.text.SimpleDateFormat("YYYY-MM-DD HH:mm:ss").format(new java.util.Date (epoch*1000));
    }

}