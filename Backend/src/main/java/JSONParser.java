/**
 *   Class that handles the parsing of the received TTN data
 *   @author: Arsenii Belyakov
 *   @version: 1.0
 **/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class JSONParser {

    private HashMap<String, String> data;

    /**
     * converts the data regarding the device to a hashmap
     * @name:        convertDevice
     * @param:       Jsonobject
     * @return:      void
     */
    private void convertDevice(JSONObject obj) {
        data.put("timestamp", Long.toString(System.currentTimeMillis()/1000));
        try {
            data.put("dev_id", String.valueOf(obj.getString("dev_id")));
            data.put("port", String.valueOf(obj.getInt("port")));
            data.put("hardware_serial", String.valueOf(obj.getString("hardware_serial")));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * converts the data rearding the actual data to a hashmap
     * @name:        convertRecived_Data
     * @param:       Jsonobject
     * @return:      void
     */
    private void convertRecived_Data (JSONObject obj) {
        try {
            //getting the data
            data.put("humidity", String.valueOf(obj.getJSONObject("payload_fields").getInt("humidity")));
            data.put("temperature", String.valueOf(obj.getJSONObject("payload_fields").getInt("temperature")));
            data.put("lux_blue", String.valueOf(obj.getJSONObject("payload_fields").getInt("lux_blue")));
            data.put("lux_red", String.valueOf(obj.getJSONObject("payload_fields").getInt("lux_red")));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * puts the data from a json string in a hashmap
     * @name:        convertAntena
     * @param:       Jsonobject
     * @return:      void
     */
    private void convertAntena(JSONObject obj) {

        JSONArray jsonArray = null;
        try {
            jsonArray = (obj.getJSONObject("metadata").getJSONArray("gateways"));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        for(int index = 0; index < jsonArray.length(); index++ ) {
            try {
                JSONObject object = jsonArray.getJSONObject(index);

                data.put("gtw_id",   object.getString("gtw_id"));
                data.put("latitude", String.valueOf(object.getDouble("latitude")));
                data.put("longitude", String.valueOf(object.getDouble("longitude")));
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * puts the data from a json string in a hashmap
     * @name:        covertData
     * @param:       String
     * @return:      void
     */
    public void covertData(String json) {
        data = new HashMap<>();

        JSONObject obj = null;
        try {
            obj = new JSONObject(json);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        assert obj != null;
        convertDevice(obj);
        convertRecived_Data(obj);
        convertAntena(obj);
    }

    /**
     * returns the hashmap containing the LoPy data
     * @name:        getData
     * @param:       none
     * @return:      HashMap<String, String>
     */
    public HashMap<String, String> getData()
    {
        return data;
    }
}