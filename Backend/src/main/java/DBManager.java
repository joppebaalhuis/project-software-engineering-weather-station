/**
 *   Class that handles the connection to the database.
 *   This class is using the singleton pattern
 *   @author: Joppe Baalhuis, Laurens Pelgrom, Timo van der Kuil
 *   @version: 1.0
 **/

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBManager {
    private static DBManager uniqueInstance = null;
    private static Connection connection = null;

    /**
     * Executes the dbExists statement, to make a connection. If false, error.
     * @name:        DBManager
     * @param:       none
     * @return:      void
     */
    public DBManager() {
        if (!dbExists()) {
            System.err.println(">>> DBManager: The database doesn't exist ...");
        }
    }

    /**
     * If there is no instance, create one. else just returns the instance
     * @name:        getInstance
     * @param:       String broker, String pass, String user, String clientId
     * @return:      void
     */
    public static synchronized DBManager getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new DBManager();
        }
        return uniqueInstance;
    }

    /**
     * creates a connection with the database by calling the
     * makeConnection() function if there is no connection.
     * @name:        dbExists
     * @param:       none
     * @return:      Boolean
     */
    private Boolean dbExists() {
        Boolean exists = false;
        Statement statement = null;
        try {
            if (connection == null){
                makeConnection();
            }
            //This statement makes sure we are using the correct database
            statement = connection.createStatement();
            statement.executeQuery("USE  sponge2;");
        }
        catch (SQLException se) {
            se.printStackTrace();
        }
        finally {
            try {
                if (statement != null) {
                    statement.close();
                    exists = true;
                }
            }
            catch (SQLException se) {
                se.printStackTrace();
                statement = null;
            }
        }
        return exists;
    }

    /**
     * Makes a connection with the database, using the properties stored in the database.properties file
     * @name:        makeConnection
     * @param:       none
     * @return:      void
     */
    public void makeConnection() {
        FileInputStream in = null;
        try {
            Properties props = new Properties();
            in = new FileInputStream("database.properties");
            props.load(in);
            in.close();

            String db_url =  props.getProperty("jdbc.db_url");
            String username = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");

            connection = DriverManager.getConnection(db_url, username, password);
            System.out.println("Connection with database established.");
        }
        catch (SQLException se) {
            System.err.println("Connection error....");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    /**
     * This function closes the connection with the database, and deletes the instance.
     * @name:        close
     * @param:       none
     * @return:      void
     */
    public void close() {
        try {
            connection.close();

            uniqueInstance = null;
            connection = null;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the Connection
     * @name:        getConnection
     * @param:       none
     * @return:      Connection
     */
    public Connection    getConnection()
    {
        return connection;
    }
}
