#Project software LORA weather station
#group 4

# library imports
from SI7006A20 import SI7006A20
from LTR329ALS01 import LTR329ALS01
from MPL3115A2 import MPL3115A2
from LIS2HH12 import LIS2HH12
from network import LoRa
import time
import binascii
import socket

#object creations
lightObject = LTR329ALS01()
temperatureObject = SI7006A20()
gyroObject = LIS2HH12()
pressureObject = MPL3115A2()        #default mode is PRESSURE can set it in altitude mode by providing ALTITUDE as argument


#variables declerations
lux         =0
temperature =0
humidity    =0
dewPoint    =0
acceleration=0
roll        =0
pitch       =0
pressure    =0
blue        =0
red         =0

def createConnectionByLora():
    lora = LoRa(mode=LoRa.LORAWAN)

    app_eui = binascii.unhexlify('70B3D57ED0014F3C')
    app_key = binascii.unhexlify('B596436D1AC408647662217B4F4096BE')

    lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)

    # wait until the module has joined the network
    while not lora.has_joined():
        time.sleep(2.5)
        print('Not joined yet...')

    print('Network joined!')

def sendByLora():
    s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
    s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
    s.setblocking(False)
    s.settimeout(2)
    s.send(bytes([int(temperature), int(humidity), int(blue), int (red)]))

#function that reads all the sensor data and stores them into variables
def getSensorData():
    global lux  	           #python way of saying that it is a global vairable same as "this.foobar" in c++
    global temperature
    global humidity
    global dewPoint
    global acceleration
    global roll
    global pitch
    global pressure
    global prev_roll
    global prev_pitch
    global prev_acceleration
    global moveFlag
    global blue
    global red

    lux         = lightObject.light()
    blue        = lux[0]
    red         = lux[1]
    temperature = temperatureObject.temperature()
    humidity    = temperatureObject.humidity()
    # dewPoint    = temperatureObject.dew_point()
    acceleration= gyroObject.acceleration()
    roll        = gyroObject.roll()
    pitch       = gyroObject.pitch()
    pressure    = pressureObject.pressure()

def setPrevMoveData():
    prev_roll = roll
    prev_pitch = pitch
    prev_acceleration = acceleration


#function that prints the sensor data for testing an debugging purpuse
def printDataToScreen():
    print("lux_blue = "          + str(blue))
    print("lux_red = "          + str(red))
    print("temperature = "  + str(temperature))
    print("humidity = "     + str(humidity))
    # print("dewPoint = "     + str(dewPoint))
    print("acceleration = " + str(acceleration))
    print("roll = "         + str(roll))
    print("pitch = "        + str(pitch))
    print("pressure = "     + str(pressure))
    print("\n")

#main loop
createConnectionByLora()
while True:
    getSensorData()
    printDataToScreen()
    setPrevMoveData()
    sendByLora()
    time.sleep(5)
#end of program
