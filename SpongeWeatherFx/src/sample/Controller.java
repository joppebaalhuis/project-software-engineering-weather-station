package sample;

/**
 * Sample Skeleton for 'sponge.fxml' Controller Class
 */

import java.net.URL;
import java.sql.Time;
import java.util.*;

import com.sun.tools.javah.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import static javafx.application.Platform.exit;

public class Controller {

    private String deviceID;


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="headBlock"
    private AnchorPane headBlock; // Value injected by FXMLLoader

    @FXML // fx:id="devicePic"
    private ImageView devicePic; // Value injected by FXMLLoader

    @FXML // fx:id="graphPic"
    private ImageView graphPic; // Value injected by FXMLLoader

    @FXML // fx:id="databasePic"
    private ImageView databasePic; // Value injected by FXMLLoader

    @FXML // fx:id="developerPic"
    private ImageView developerPic; // Value injected by FXMLLoader

    @FXML // fx:id="exitPic"
    private ImageView exitPic; // Value injected by FXMLLoader

    @FXML // fx:id="deviceArrow"
    private ImageView deviceArrow; // Value injected by FXMLLoader

    @FXML // fx:id="graphArrow"
    private ImageView graphArrow; // Value injected by FXMLLoader

    @FXML // fx:id="tableArrow"
    private ImageView tableArrow; // Value injected by FXMLLoader

    @FXML // fx:id="creatorsArrow"
    private ImageView creatorsArrow; // Value injected by FXMLLoader

    @FXML // fx:id="creatorsPage"
    private AnchorPane creatorsPage; // Value injected by FXMLLoader

    @FXML // fx:id="tablesPage"
    private AnchorPane tablesPage; // Value injected by FXMLLoader

    @FXML // fx:id="table"
    private TableView<TablesDataType> table; // Value injected by FXMLLoader

    @FXML // fx:id="table_Id"
    private TableColumn<TablesDataType, String> table_Id; // Value injected by FXMLLoader

    @FXML // fx:id="table_Time"
    private TableColumn<TablesDataType, String> table_Time; // Value injected by FXMLLoader

    @FXML // fx:id="table_Topic"
    private TableColumn<TablesDataType, String> table_Topic; // Value injected by FXMLLoader

    @FXML // fx:id="table_Data"
    private TableColumn<TablesDataType, String> table_Data; // Value injected by FXMLLoader

    @FXML // fx:id="tablesCheckTemperature"
    private CheckBox tablesCheckTemperature; // Value injected by FXMLLoader

    @FXML // fx:id="checkHumidity1"
    private CheckBox checkHumidity1; // Value injected by FXMLLoader

    @FXML // fx:id="tablesCheckLux_R"
    private CheckBox tablesCheckLux_R; // Value injected by FXMLLoader

    @FXML // fx:id="tablesCheckLux_B"
    private CheckBox tablesCheckLux_B; // Value injected by FXMLLoader

    @FXML // fx:id="tablesEnableAll"
    private Button tablesEnableAll; // Value injected by FXMLLoader

    @FXML // fx:id="tablesDisableAll"
    private Button tablesDisableAll; // Value injected by FXMLLoader

    @FXML // fx:id="graphPage"
    private AnchorPane graphPage; // Value injected by FXMLLoader

    @FXML // fx:id="graph"
    private LineChart<?, ?> graph; // Value injected by FXMLLoader

    @FXML // fx:id="x"
    private CategoryAxis x; // Value injected by FXMLLoader

    @FXML // fx:id="y"
    private NumberAxis y; // Value injected by FXMLLoader

    @FXML // fx:id="graphCheckTemperature"
    private CheckBox graphCheckTemperature; // Value injected by FXMLLoader

    @FXML // fx:id="graphCheckHumidity"
    private CheckBox graphCheckHumidity; // Value injected by FXMLLoader

    @FXML // fx:id="graphCheckLux_R"
    private CheckBox graphCheckLux_R; // Value injected by FXMLLoader

    @FXML // fx:id="graphCheckLux_B"
    private CheckBox graphCheckLux_B; // Value injected by FXMLLoader

    @FXML // fx:id="graphEnableAll"
    private Button graphEnableAll; // Value injected by FXMLLoader

    @FXML // fx:id="graphDisableAll"
    private Button graphDisableAll; // Value injected by FXMLLoader

    @FXML // fx:id="devicePage"
    private AnchorPane devicePage; // Value injected by FXMLLoader

    @FXML // fx:id="deviceChoiseBox"
    private Text deviceChoiseBox; // Value injected by FXMLLoader

    @FXML // fx:id="deviceText"
    private TextField deviceText; // Value injected by FXMLLoader

    @FXML // fx:id="deviceUpdate"
    private Button deviceUpdate; // Value injected by FXMLLoader

    @FXML
    private ChoiceBox deviceChoisebox;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private NumberAxis xAcis;


    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert headBlock != null : "fx:id=\"headBlock\" was not injected: check your FXML file 'sponge.fxml'.";
        assert devicePic != null : "fx:id=\"devicePic\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphPic != null : "fx:id=\"graphPic\" was not injected: check your FXML file 'sponge.fxml'.";
        assert databasePic != null : "fx:id=\"databasePic\" was not injected: check your FXML file 'sponge.fxml'.";
        assert developerPic != null : "fx:id=\"developerPic\" was not injected: check your FXML file 'sponge.fxml'.";
        assert exitPic != null : "fx:id=\"exitPic\" was not injected: check your FXML file 'sponge.fxml'.";
        assert deviceArrow != null : "fx:id=\"deviceArrow\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphArrow != null : "fx:id=\"graphArrow\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tableArrow != null : "fx:id=\"tableArrow\" was not injected: check your FXML file 'sponge.fxml'.";
        assert creatorsArrow != null : "fx:id=\"creatorsArrow\" was not injected: check your FXML file 'sponge.fxml'.";
        assert creatorsPage != null : "fx:id=\"creatorsPage\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tablesPage != null : "fx:id=\"tablesPage\" was not injected: check your FXML file 'sponge.fxml'.";
        assert table != null : "fx:id=\"table\" was not injected: check your FXML file 'sponge.fxml'.";
        assert table_Id != null : "fx:id=\"table_Id\" was not injected: check your FXML file 'sponge.fxml'.";
        assert table_Time != null : "fx:id=\"table_Time\" was not injected: check your FXML file 'sponge.fxml'.";
        assert table_Topic != null : "fx:id=\"table_Topic\" was not injected: check your FXML file 'sponge.fxml'.";
        assert table_Data != null : "fx:id=\"table_Data\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tablesCheckTemperature != null : "fx:id=\"tablesCheckTemperature\" was not injected: check your FXML file 'sponge.fxml'.";
        assert checkHumidity1 != null : "fx:id=\"checkHumidity1\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tablesCheckLux_R != null : "fx:id=\"tablesCheckLux_R\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tablesCheckLux_B != null : "fx:id=\"tablesCheckLux_B\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tablesEnableAll != null : "fx:id=\"tablesEnableAll\" was not injected: check your FXML file 'sponge.fxml'.";
        assert tablesDisableAll != null : "fx:id=\"tablesDisableAll\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphPage != null : "fx:id=\"graphPage\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graph != null : "fx:id=\"graph\" was not injected: check your FXML file 'sponge.fxml'.";
        assert x != null : "fx:id=\"x\" was not injected: check your FXML file 'sponge.fxml'.";
        assert y != null : "fx:id=\"y\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphCheckTemperature != null : "fx:id=\"graphCheckTemperature\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphCheckHumidity != null : "fx:id=\"graphCheckHumidity\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphCheckLux_R != null : "fx:id=\"graphCheckLux_R\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphCheckLux_B != null : "fx:id=\"graphCheckLux_B\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphEnableAll != null : "fx:id=\"graphEnableAll\" was not injected: check your FXML file 'sponge.fxml'.";
        assert graphDisableAll != null : "fx:id=\"graphDisableAll\" was not injected: check your FXML file 'sponge.fxml'.";
        assert devicePage != null : "fx:id=\"devicePage\" was not injected: check your FXML file 'sponge.fxml'.";
        assert deviceChoiseBox != null : "fx:id=\"deviceChoiseBox\" was not injected: check your FXML file 'sponge.fxml'.";
        assert deviceText != null : "fx:id=\"deviceText\" was not injected: check your FXML file 'sponge.fxml'.";
        assert deviceUpdate != null : "fx:id=\"deviceUpdate\" was not injected: check your FXML file 'sponge.fxml'.";
        assert deviceChoisebox != null : "fx:id=\"deviceChoisebox\" was not injected: check your FXML file 'sponge.fxml'.";
    }

//    public void tablesAdd(){
//
//        final ObservableList<Data> data = FXCollections.observableArrayList(
//           new Data(id,time,topic,data);
//                new Data("1","Jan","Temperature","-10"),
//                new Data("2","Feb","Temperature","14"),
//                new Data("3","Mar","Temperature","15"),
//                new Data("1","Jan","Humidity","33"),
//                new Data("2","Feb","Humidity","34"),
//                new Data("3","Mar","Humidity","25"),
//                new Data("2","Jan","Lux_B","44"),
//                new Data("2","Feb","Lux_B","44")
//                );
//        table_Id.setCellValueFactory(new PropertyValueFactory<Data,String>("id"));
//        table_Time.setCellValueFactory(new PropertyValueFactory<Data,String>("time"));
//        table_Topic.setCellValueFactory(new PropertyValueFactory<Data,String>("topic"));
//        table_Data.setCellValueFactory(new PropertyValueFactory<Data,String>("data"));
//
//
//
//        table.setItems(data);
//    }

    private Timer timer = new Timer();


    public void LineChart(){
        Data data = new Data();

        data.setNames();
        data.addAllData();

        graph.setCreateSymbols(false);

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.err.println("Update each 10 min");
                data.addNewestData();
            }
        }, 1000, 600000);


        if (graphCheckTemperature.isSelected()) graph.getData().addAll(data.getSeries1());
        if (graphCheckHumidity.isSelected()) graph.getData().addAll(data.getSeries2());
        if(graphCheckLux_B.isSelected()) graph.getData().addAll(data.getSeries3());
        if (graphCheckLux_R.isSelected()) graph.getData().addAll(data.getSeries4());
    }


    private int counter = 0;
    public void devicePicPress() {
        System.out.println("devicePicPress");
        deviceArrow.setVisible(true);
        graphArrow.setVisible(false);
        tableArrow.setVisible(false);
        creatorsArrow.setVisible(false);

        devicePage.setDisable(false);
        graphPage.setDisable(true);
        tablesPage.setDisable(true);
        creatorsPage.setDisable(true);

        devicePage.setVisible(true);
        graphPage.setVisible(false);
        tablesPage.setVisible(false);
        creatorsPage.setVisible(false);
        counter++;
        deviceChoisebox.getItems().addAll("lopyweatherstation"+counter);
        deviceText.setText("Here will be info about your lopyweatherstation");
        deviceText.setEditable(false);

    }

    public LineChart<?, ?> getGraph() {
        return graph;
    }

    public void graphPicPress() {
        System.out.println("graphPicPress");
        deviceArrow.setVisible(false);
        graphArrow.setVisible(true);
        tableArrow.setVisible(false);
        creatorsArrow.setVisible(false);

        graph.getData().removeAll(Collections.singleton(graph.getData().setAll()));

        graph.setAnimated(false);

        LineChart();

        devicePage.setDisable(true);
        graphPage.setDisable(false);
        tablesPage.setDisable(true);
        creatorsPage.setDisable(true);

        devicePage.setVisible(false);
        graphPage.setVisible(true);
        tablesPage.setVisible(false);
        creatorsPage.setVisible(false);

    }

    public void databasePicPress() {
        System.out.println("databasePicPress");
        deviceArrow.setVisible(false);
        graphArrow.setVisible(false);
        tableArrow.setVisible(true);
        creatorsArrow.setVisible(false);

        devicePage.setDisable(true);
        graphPage.setDisable(true);
        tablesPage.setDisable(false);
        creatorsPage.setDisable(true);

        devicePage.setVisible(false);
        graphPage.setVisible(false);
        tablesPage.setVisible(true);
        creatorsPage.setVisible(false);

        TabelsData tabelsData = new TabelsData();

        tabelsData.fillTables();

        table_Id.setCellValueFactory(new PropertyValueFactory<>("id"));
        table_Time.setCellValueFactory(new PropertyValueFactory<>("time"));
        table_Topic.setCellValueFactory(new PropertyValueFactory<>("topic"));
        table_Data.setCellValueFactory(new PropertyValueFactory<>("data"));

        table.setItems(tabelsData.getData());

    }

    public void developerPicPress() {
        System.out.println("developerPicPress");
        deviceArrow.setVisible(false);
        graphArrow.setVisible(false);
        tableArrow.setVisible(false);
        creatorsArrow.setVisible(true);

        devicePage.setDisable(true);
        graphPage.setDisable(true);
        tablesPage.setDisable(true);
        creatorsPage.setDisable(false);

        devicePage.setVisible(false);
        graphPage.setVisible(false);
        tablesPage.setVisible(false);
        creatorsPage.setVisible(true);


    }

    public void exitPicPress() {
        System.out.println("exitPicPress");
        deviceArrow.setVisible(false);
        graphArrow.setVisible(false);
        tableArrow.setVisible(false);
        creatorsArrow.setVisible(false);
        System.exit(0);
    }

    public void graphEnableAllPress() {
        System.out.println("graphEnableAllPress");
        graphCheckTemperature.setSelected(true);
        graphCheckHumidity.setSelected(true);
        graphCheckLux_R.setSelected(true);
        graphCheckLux_B.setSelected(true);
    }

    public void graphDisableAllPress() {
        System.out.println("graphDisableAllPress");
        graphCheckTemperature.setSelected(false);
        graphCheckHumidity.setSelected(false);
        graphCheckLux_R.setSelected(false);
        graphCheckLux_B.setSelected(false);
    }

    public void tablesEnableAllPress() {
        System.out.println("tablesEnableAllPress");
        tablesCheckTemperature.setSelected(true);
        checkHumidity1.setSelected(true);
        tablesCheckLux_B.setSelected(true);
        tablesCheckLux_R.setSelected(true);
    }

    public void tablesDisableAllPress() {
        System.out.println("tablesDisableAllPress");
        tablesCheckTemperature.setSelected(false);
        checkHumidity1.setSelected(false);
        tablesCheckLux_B.setSelected(false);
        tablesCheckLux_R.setSelected(false);
    }

    public void deviceUpdatePress() {
        System.out.println("deviceUpdatePress");
        devicePicPress();
    }


    public void graphCheck(ActionEvent actionEvent) {

        graph.getData().removeAll(Collections.singleton(graph.getData().setAll()));
        LineChart();
    }

    public void tablesCheck(ActionEvent actionEvent) {
        System.out.println("tables check");
    }
}
