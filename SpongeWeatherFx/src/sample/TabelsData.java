package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Collection;
import java.util.TreeMap;

public class TabelsData {

    private TreeMap<String, TreeMap<String,String>> allData = new TreeMap<>();

    private ObservableList<TablesDataType> data = FXCollections.observableArrayList();

    public TreeMap<String, TreeMap<String, String>> getAllData() {
        return allData;
    }

    public ObservableList<TablesDataType> getData() {
        return data;
    }

    public void fillTables(){
        ConnectionHandler ch = new ConnectionHandler();
        allData = ch.getAllData();

        for (String timestamp: allData.keySet()) {
            for (String key:allData.get(timestamp).keySet()) {
                System.out.println("lopyweatherstation\t||\t"+timestamp +"\t||\t"+ key +"\t||\t"+ allData.get(timestamp).get(key));
                data.add(new TablesDataType("lopyweatherstation", timestamp , key ,allData.get(timestamp).get(key)));
            }
        }
        System.err.println("/////////////////////////////////////////// \n ////////////////////////////////////////// \n ////////////////////////////////////////////////////////////////////////////////////");
        data.forEach(k-> System.out.println(k));
    }
}
