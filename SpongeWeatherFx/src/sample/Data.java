package sample;

import javafx.scene.chart.XYChart;

import java.util.TreeMap;

public class Data {
    private XYChart.Series series1 = new XYChart.Series();
    private XYChart.Series series2 = new XYChart.Series();
    private XYChart.Series series3 = new XYChart.Series();
    private XYChart.Series series4 = new XYChart.Series();

    private TreeMap<String, TreeMap< String, String>> allData = new TreeMap<>();

    public TreeMap<String, TreeMap<String, String>> getAllData() {
        return allData;
    }

    public void setNames(){
        series1.setName("Temperature");
        series2.setName("Humidity");
        series3.setName("Lux_B");
        series4.setName("Lux_R");
    }

    public XYChart.Series getSeries1() {
        return series1;
    }

    public XYChart.Series getSeries2() {
        return series2;
    }

    public XYChart.Series getSeries3() {
        return series3;
    }

    public XYChart.Series getSeries4() {
        return series4;
    }

    public void presetData(){
        series1.getData().add(new XYChart.Data("Jan", -10));
        series1.getData().add(new XYChart.Data("Feb", 14));
        series1.getData().add(new XYChart.Data("Mar", 15));
        series1.getData().add(new XYChart.Data("Apr", 24));
        series1.getData().add(new XYChart.Data("May", 34));
        series1.getData().add(new XYChart.Data("Jun", 36));
        series1.getData().add(new XYChart.Data("Jul", 22));
        series1.getData().add(new XYChart.Data("Aug", 45));
        series1.getData().add(new XYChart.Data("Sep", 43));
        series1.getData().add(new XYChart.Data("Oct", 17));
        series1.getData().add(new XYChart.Data("Nov", 29));
        series1.getData().add(new XYChart.Data("Dec", 25));

        series2.getData().add(new XYChart.Data("Jan", 33));
        series2.getData().add(new XYChart.Data("Feb", 34));
        series2.getData().add(new XYChart.Data("Mar", 25));
        series2.getData().add(new XYChart.Data("Apr", 44));
        series2.getData().add(new XYChart.Data("May", 39));
        series2.getData().add(new XYChart.Data("Jun", 16));
        series2.getData().add(new XYChart.Data("Jul", 55));
        series2.getData().add(new XYChart.Data("Aug", 54));
        series2.getData().add(new XYChart.Data("Sep", 48));
        series2.getData().add(new XYChart.Data("Oct", 27));
        series2.getData().add(new XYChart.Data("Nov", 37));
        series2.getData().add(new XYChart.Data("Dec", 29));


        series3.getData().add(new XYChart.Data("Jan", 44));
        series3.getData().add(new XYChart.Data("Feb", 35));
        series3.getData().add(new XYChart.Data("Mar", 36));
        series3.getData().add(new XYChart.Data("Apr", 33));
        series3.getData().add(new XYChart.Data("May", 31));
        series3.getData().add(new XYChart.Data("Jun", 26));
        series3.getData().add(new XYChart.Data("Jul", 22));
        series3.getData().add(new XYChart.Data("Aug", 25));
        series3.getData().add(new XYChart.Data("Sep", 43));
        series3.getData().add(new XYChart.Data("Oct", 44));
        series3.getData().add(new XYChart.Data("Nov", 45));
        series3.getData().add(new XYChart.Data("Dec", 44));


        series4.getData().add(new XYChart.Data("Jan", 34));
        series4.getData().add(new XYChart.Data("Feb", 32));
        series4.getData().add(new XYChart.Data("Mar", 30));
        series4.getData().add(new XYChart.Data("Apr", 0));
        series4.getData().add(new XYChart.Data("May", 7));
        series4.getData().add(new XYChart.Data("Jun", 6));
        series4.getData().add(new XYChart.Data("Jul", 27));
        series4.getData().add(new XYChart.Data("Aug", 26));
        series4.getData().add(new XYChart.Data("Sep", -3));
        series4.getData().add(new XYChart.Data("Oct", 43));
        series4.getData().add(new XYChart.Data("Nov", 42));
        series4.getData().add(new XYChart.Data("Dec", 4));

    }

    public void addNewestData(){
                ConnectionHandler ch = new ConnectionHandler();
        TreeMap myData = ch.getLatestData();
        for (Object timestamp : myData.keySet()) {
                allData.put(timestamp.toString(),myData);
        }

        for (String timestamp : allData.keySet()) {
            for (String key: allData.get(timestamp).keySet()) {
                switch (key) {
                    case "Temperature":
                        series1.getData().add(new XYChart.Data(timestamp, allData.get(timestamp).get(key)));
                        break;
                    case "Humidity":
                        series2.getData().add(new XYChart.Data(timestamp, allData.get(timestamp).get(key)));
                        break;
                    case "Lux_B":
                        series3.getData().add(new XYChart.Data(timestamp, allData.get(timestamp).get(key)));
                        break;
                    case "Lux_R":
                        series4.getData().add(new XYChart.Data(timestamp, allData.get(timestamp).get(key)));
                        break;
                    case "default":
                        System.err.println("Wrong data in the HashMap");
                        break;
                }

            }
        }
    }


    public void addAllData(){
        ConnectionHandler ch = new ConnectionHandler();
        allData = ch.getAllData();
        for (String timestamp : allData.keySet()) {
            for (String key: allData.get(timestamp).keySet()) {
                System.out.println(key+"\t"+allData.get(timestamp).get(key));
                switch (key) {
                    case "temperature":
                        series1.getData().add(new XYChart.Data(timestamp, Integer.parseInt(allData.get(timestamp).get(key))));
                        System.out.println(key+"\t"+allData.get(timestamp).get(key));
                        break;
                    case "humidity":
                        series2.getData().add(new XYChart.Data(timestamp, Integer.parseInt(allData.get(timestamp).get(key))));
                        System.out.println(key+"\t"+allData.get(timestamp).get(key));
                        break;
                    case "lux_blue":
                        series3.getData().add(new XYChart.Data(timestamp,Integer.parseInt(allData.get(timestamp).get(key))));
                        System.out.println(key+"\t"+allData.get(timestamp).get(key));
                        break;
                    case "lux_red":
                        series4.getData().add(new XYChart.Data(timestamp, Integer.parseInt(allData.get(timestamp).get(key))));
                        System.out.println(key+"\t"+allData.get(timestamp).get(key));
                    case "default":
                        System.err.println("Wrong data in the HashMap");
                        break;
                }

            }
        }
    }
}
