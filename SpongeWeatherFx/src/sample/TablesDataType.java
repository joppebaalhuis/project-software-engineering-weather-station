package sample;

public class TablesDataType {
    private String id;
    private String timestamp;
    private String type;
    private String value;

    public TablesDataType(String id, String timestamp, String type, String value) {
        this.id = id;
        this.timestamp = timestamp;
        this.type = type;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
