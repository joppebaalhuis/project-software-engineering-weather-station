package sample;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.net.*;
import java.io.*;

public class ConnectionHandler {
    private String hostname = "localhost";
    private int port = 8000;
    private Timer timer;
    private int heardBeatInterval = 2;

    private Socket connectToServer() {
        Socket socket = null;
        try {
            socket = new Socket(hostname, port);
        } catch (UnknownHostException ex) {

            System.out.println("Server not found: " + ex.getMessage());

        } catch (IOException ex) {

            System.out.println("I/O error: " + ex.getMessage());
        }
        return socket;
    }

    //TODO: write more generic code
    public TreeMap<String, TreeMap<String, String>> getAllData() {
        TreeMap<String, TreeMap<String, String>> dataMap = new TreeMap<String, TreeMap<String, String>>();
        Socket socket = connectToServer();
        try {
            DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
            String dataFromServer = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            dOut.writeBytes("getAllData\n");
            dOut.flush(); // Send off the d
            dataFromServer = reader.readLine();

            try
            {
                JSONArray json = new JSONArray(dataFromServer);

                for (int i = 0; i < json.length(); i++){
                    JSONObject upper = json.getJSONObject(i);
                    JSONObject lower = upper.getJSONObject("Data");

                    TreeMap<String, String> data = new TreeMap<String, String>();
                    data.put("Device_id", lower.getString("Device_id"));
                    data.put("temperature", lower.getString("temperature"));
                    data.put("humidity", lower.getString("humidity"));
                    data.put("counter", lower.getString("counter"));
                    data.put("lux_blue", lower.getString("lux_blue"));
                    data.put("lux_red", lower.getString("lux_red"));
                    dataMap.put(upper.getString("Timestamp"), data);
                }
            }
            catch (JSONException e)
            {
                // TODO Handle expection!
            }

            //   dataHashmap.put("device1", json.getString("device1"));

            return dataMap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;            //silence the compiler
    }


    public TreeMap<String, TreeMap<String, String>> getLatestData() {
        Socket socket = connectToServer();
        try {
            DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
            String dataFromServer = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            dOut.writeBytes("getLatestData \n");
            dOut.flush(); // Send off the d
            dataFromServer = reader.readLine();
            TreeMap<String, TreeMap<String, String>> dataMap = new TreeMap<String, TreeMap<String, String>>();
            TreeMap<String, String> data = new TreeMap<String, String>();
            JSONObject json = new JSONObject(dataFromServer);

            data.put("Device_id", json.getString("Device_id"));
            data.put("temperature", json.getString("temperature"));
            data.put("humidity", json.getString("humidity"));
            data.put("counter", json.getString("counter"));
            data.put("lux_blue", json.getString("lux_blue"));
            data.put("lux_red", json.getString("lux_red"));
            dataMap.put(json.getString("timestamp"), data);
            return dataMap;

        } catch (JSONException e) {

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }

}
