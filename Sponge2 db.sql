create database sponge2;

use sponge2;

CREATE TABLE Recived_Data (
    device_id VARCHAR(20) NOT NULL,
    counter INT PRIMARY KEY AUTO_INCREMENT ,
    timestamp datetime NOT NULL,
    temperature INT NOT NULL,
    humidity INT NOT NULL,
    lux_red INT NOT NULL,
    lux_blue INT NOT NULL
);
